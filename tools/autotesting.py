import sys
import os
import hammer_config as config

usage = """
Usage:
    hammer autotesting [Robot Framework options...]
            To process automation tests.

            Robot Framework options are directly passed to robot if they are present.
            See https://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#command-line-options-for-test-execution

            The most likely options are:
            --suite <name>
                    Selects the test suites by name.

            --test <name>
                    Selects the test cases by name.
            
Or:
    hammer autotesting <-h|--help>
            To display this help.
"""

args = sys.argv[1:-1]

if '-h' in args or '--help' in args:
    print(usage)
    exit(0)

if '--outputdir' in args or '-d' in args:
    print('Error: You should not directly specify an output directory.', file=sys.stderr)
    print(usage)
    sys.exit(255)

robot_cmd = 'robot -d {outdir} {options} {testdir}'.format(outdir=os.path.join(config.outdir, 'autotesting'), options=' '.join(args), testdir=os.path.join(config.srcdir, 'autotesting', 'tests'))

print(robot_cmd)
errorcode = os.system(robot_cmd)
sys.exit(errorcode)