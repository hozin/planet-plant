import sys
import os

def __config_file_path():
    hammer_config = None
    for arg in sys.argv[1:]:
        if arg.startswith('--hammer_config='):
            _, hammer_config = arg.split('=')

    if hammer_config is None:
        print('Internal error: - hammer config has not been specified!', file=sys.stderr)
        sys.exit(255)

    if not os.path.isfile(hammer_config):
        print('Error: Config file "{}" has not been found.'.format(hammer_config), file=sys.stderr)
        sys.exit(255)
    
    return os.path.abspath(hammer_config)

def __read_values(config_path):
    values = {}
    with open(config_path, 'r') as cfg_file:
        for line in cfg_file.readlines():
            line = line.strip()
            if line == '' or line.startswith('#'):
                continue
            key, value = line.split('=')
            values[key.strip()] = value.strip()
    return values

def __normalize_path(root, path):
    return os.path.normpath(os.path.join(root, path))

def __read():
    config_path = __config_file_path()
    values = __read_values(config_path)
    rootdir = os.path.dirname(config_path)
    outdir = __normalize_path(rootdir, values.pop('OUTDIR'))
    srcdir = __normalize_path(rootdir, values.pop('SRCDIR'))
    pkgdir = __normalize_path(rootdir, values.pop('PKGDIR')) if 'PKGDIR' in values.keys() else None
    pkgcfg = __normalize_path(rootdir, values.pop('PKGCFG')) if 'PKGCFG' in values.keys() else None
    buildcfg = values.pop('CONFIGURATION') if 'CONFIGURATION' in values.keys() else None
    buildopt = values.pop('BUILD_OPTIONS') if 'BUILD_OPTIONS' in values.keys() else None
    return (rootdir, srcdir, outdir, pkgdir, pkgcfg, buildcfg, buildopt, values)

rootdir, srcdir, outdir, pkgdir, pkgcfg, buildcfg, buildopt, values = __read()