import sys
import os
import hammer_config as config

usage = """
Usage:
    hammer make config [options...]
            To generate project buildsystem that is used by native build tool to build the project.

            Options are directly passed to cmake and can be one or several allowable cmake options
            excluding both -S and -B that are overrided by this build system.
            
Or:
    hammer make config <-h|--help>
            To display this help.
"""

args = sys.argv[1:-1]

if '-h' in args or '--help' in args:
    print(usage)
    exit(0)

if '-S' in args or '-B' in args:
    print('Error: You should not directly specify a source directory or an output directory.', file=sys.stderr)
    print(usage)
    sys.exit(255)

defines = ['-D {}={}'.format(k, v) for k, v in config.values.items()]

cmake_cmd = 'cmake {options} {defines} -S {srcdir} -B {bindir}'.format(srcdir=config.srcdir, bindir=config.outdir, defines=' '.join(defines), options=' '.join(args))

print(cmake_cmd)
errorcode = os.system(cmake_cmd)
sys.exit(errorcode)