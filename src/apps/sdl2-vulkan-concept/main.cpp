static_assert(sizeof(unsigned char) == 1);
#include "shaders/frag.h"
static_assert(shader_frag_length % 4 == 0);
#include "shaders/vert.h"
static_assert(shader_vert_length % 4 == 0);

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

#define VK_NO_PROTOTYPES
#include <vulkan/vulkan.hpp>

#include <fmt/core.h>

#include <boost/algorithm/string.hpp>

#include <iostream>
#include <stdexcept>
#include <memory>
#include <vector>
#include <string>
#include <optional>
#include <set>

constexpr auto SUCCESS = 0;
constexpr auto FAILURE = 255;

class SdlInit {
 public:
    explicit SdlInit(Uint32 flags) {
        if (SDL_Init(flags) != 0) {
            throw std::runtime_error(fmt::format("Unable to initialize SDL: {}", SDL_GetError()));
        }
    }
    ~SdlInit() {
        SDL_Quit();
    }
};

class VulkanLibrary {
 public:
    VulkanLibrary() {
        if (SDL_Vulkan_LoadLibrary(nullptr) != 0) {
            throw std::runtime_error(fmt::format("Unable to load Vulkan library: {}", SDL_GetError()));
        }
        loader_.init((PFN_vkGetInstanceProcAddr)SDL_Vulkan_GetVkGetInstanceProcAddr());
        // TODO: check pointer to vkGetInstanceProcAddr
    }
    ~VulkanLibrary() {
        SDL_Vulkan_UnloadLibrary();
    }

    void init(VkInstance instance) {
        loader_.init(instance, loader_.vkGetInstanceProcAddr);
    }

    const vk::DispatchLoaderDynamic* get() const {
        return &loader_;
    }

    const vk::DispatchLoaderDynamic* operator->() const {
        return get();
    }

 private:
    vk::DispatchLoaderDynamic loader_;
};

enum class Validation { Disabled, Enabled };

class HelloTriangleApplication {
    using sdl_window_ptr = std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)>;

    static constexpr const char* applicaion_name = "SDL2 Vulkan Concept";
    static const std::vector<const char*> validation_layers;
    static const std::vector<const char*> device_extensions;
    static const int MAX_FRAMES_IN_FLIGHT = 2;

    struct QueueFamilyIndices {
        std::optional<uint32_t> graphics_family;
        std::optional<uint32_t> present_family;

        bool isComplete() const {
            return graphics_family.has_value() && present_family.has_value();
        }
    };

    struct SwapChainSupportDetails {
        VkSurfaceCapabilitiesKHR capabilities;
        std::vector<VkSurfaceFormatKHR> formats;
        std::vector<VkPresentModeKHR> present_modes;
    };

 public:
    explicit HelloTriangleApplication(Validation validation)
        : validation_(validation), sdl_init_(SDL_INIT_VIDEO | SDL_INIT_EVENTS), window_(createWindow()) {
        initVulkan();
    }
    HelloTriangleApplication(const HelloTriangleApplication&) = delete;

    ~HelloTriangleApplication() {
        cleanup();
    }

    void run() {
        mainLoop();
    }

 private:
    static sdl_window_ptr createWindow() {
        sdl_window_ptr window(SDL_CreateWindow(applicaion_name, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640,
                                               480, SDL_WINDOW_SHOWN | SDL_WINDOW_VULKAN | SDL_WINDOW_RESIZABLE),
                              SDL_DestroyWindow);
        if (!window) {
            throw std::runtime_error(fmt::format("Could not create window: {}", SDL_GetError()));
        }
        return window;
    }

    static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT /*severity*/,
                                                        VkDebugUtilsMessageTypeFlagsEXT /*type*/,
                                                        const VkDebugUtilsMessengerCallbackDataEXT* callback_data,
                                                        void* /*user_data*/) {
        std::cerr << "Validation layer: " << callback_data->pMessage << std::endl;
        return VK_FALSE;
    }

    void windowEventHandle(const SDL_WindowEvent& window) {
        switch (window.event) {
            case SDL_WINDOWEVENT_SHOWN:
                std::cout << "Window shown" << std::endl;
                window_minimized_ = false;
                break;
            case SDL_WINDOWEVENT_HIDDEN:
                std::cout << "Window hidden" << std::endl;
                break;
            case SDL_WINDOWEVENT_EXPOSED:
                std::cout << "Window exposed" << std::endl;
                break;
            case SDL_WINDOWEVENT_MOVED:
                std::cout << fmt::format("Window moved to {}, {}", window.data1, window.data2) << std::endl;
                break;
            case SDL_WINDOWEVENT_RESIZED:
                std::cout << fmt::format("Window resized to {} x {}", window.data1, window.data2) << std::endl;
                window_resized_ = true;
                break;
            case SDL_WINDOWEVENT_SIZE_CHANGED:
                std::cout << fmt::format("Window size changed to {} x {}", window.data1, window.data2) << std::endl;
                break;
            case SDL_WINDOWEVENT_MINIMIZED:
                std::cout << "Window minimized" << std::endl;
                window_minimized_ = true;
                break;
            case SDL_WINDOWEVENT_MAXIMIZED:
                std::cout << "Window maximized" << std::endl;
                break;
            case SDL_WINDOWEVENT_RESTORED:
                std::cout << "Window restored" << std::endl;
                window_minimized_ = false;
                break;
            case SDL_WINDOWEVENT_ENTER:
                std::cout << "Mouse entered window" << std::endl;
                break;
            case SDL_WINDOWEVENT_LEAVE:
                std::cout << "Mouse left window" << std::endl;
                break;
            case SDL_WINDOWEVENT_FOCUS_GAINED:
                std::cout << "Window gained keyboard focus" << std::endl;
                break;
            case SDL_WINDOWEVENT_FOCUS_LOST:
                std::cout << "Window lost keyboard focus" << std::endl;
                break;
            case SDL_WINDOWEVENT_CLOSE:
                std::cout << "Window closed" << std::endl;
                break;
                //#if SDL_VERSION_ATLEAST(2, 0, 5)
            case SDL_WINDOWEVENT_TAKE_FOCUS:
                std::cout << "Window is offered a focus" << std::endl;
                break;
            case SDL_WINDOWEVENT_HIT_TEST:
                std::cout << "Window has a special hit test" << std::endl;
                break;
                //#endif
            default:
                std::cout << fmt::format("Window got unknown event {}", window.event) << std::endl;
                break;
        }
    }

    void initVulkan() {
        createInstance();
        listExtensions();
        setupDebugMessenger();
        createSurface();
        pickPhysicalDevice();
        createLogicalDevice();
        createSwapChain();
        createImageViews();
        createRenderPass();
        createGraphicsPipeline();
        createFramebuffers();
        createCommandPool();
        createCommandBuffers();
        createSyncObjects();
    }

    void recreateSwapChain() {
        if (window_minimized_) {
            return;
        }

        vulkan_lib_->vkDeviceWaitIdle(device_);

        cleanupSwapChain();

        createSwapChain();
        createImageViews();
        createRenderPass();
        createGraphicsPipeline();
        createFramebuffers();
        createCommandBuffers();
    }

    void cleanupSwapChain() {
        for (auto framebuffer : swap_chain_framebuffers_) {
            vulkan_lib_->vkDestroyFramebuffer(device_, framebuffer, nullptr);
        }
        swap_chain_framebuffers_.clear();

        if (!command_buffers_.empty()) {
            vulkan_lib_->vkFreeCommandBuffers(device_, command_pool_, static_cast<uint32_t>(command_buffers_.size()),
                                              command_buffers_.data());
            command_buffers_.clear();
        }

        if (graphics_pipeline_) {
            vulkan_lib_->vkDestroyPipeline(device_, graphics_pipeline_, nullptr);
            graphics_pipeline_ = nullptr;
        }

        if (pipeline_layout_) {
            vulkan_lib_->vkDestroyPipelineLayout(device_, pipeline_layout_, nullptr);
            pipeline_layout_ = nullptr;
        }

        if (render_pass_) {
            vulkan_lib_->vkDestroyRenderPass(device_, render_pass_, nullptr);
            render_pass_ = nullptr;
        }

        for (auto image_view : swap_chain_image_views_) {
            vulkan_lib_->vkDestroyImageView(device_, image_view, nullptr);
        }
        swap_chain_image_views_.clear();

        if (swap_chain_) {
            vulkan_lib_->vkDestroySwapchainKHR(device_, swap_chain_, nullptr);
            swap_chain_ = nullptr;
        }
    }

    bool checkValidationLayerSupport() {
        uint32_t layer_count;
        vulkan_lib_->vkEnumerateInstanceLayerProperties(&layer_count, nullptr);
        std::vector<VkLayerProperties> available_layers(layer_count);
        vulkan_lib_->vkEnumerateInstanceLayerProperties(&layer_count, available_layers.data());

        for (const char* layer_name : validation_layers) {
            for (const auto& layer_properties : available_layers) {
                if (boost::equals(layer_name, layer_properties.layerName)) {
                    return true;
                }
            }
        }

        return false;
    }

    auto getRequiredExtensions() {
        unsigned extensions_count = 0;
        if (SDL_Vulkan_GetInstanceExtensions(window_.get(), &extensions_count, nullptr) != SDL_TRUE) {
            throw std::runtime_error(fmt::format("Could not get vulkan extensions count: {}", SDL_GetError()));
        }
        std::vector<const char*> extensions(extensions_count);
        if (SDL_Vulkan_GetInstanceExtensions(window_.get(), &extensions_count, extensions.data()) != SDL_TRUE) {
            throw std::runtime_error(fmt::format("Could not get vulkan extensions names: {}", SDL_GetError()));
        }
        if (validation_ == Validation::Enabled) {
            extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
        }
        return extensions;
    }

    void createInstance() {
        if (validation_ == Validation::Enabled && !checkValidationLayerSupport()) {
            throw std::runtime_error("Validation layers requested, but not available!");
        }

        VkApplicationInfo app_info{};
        app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        app_info.pApplicationName = applicaion_name;
        app_info.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
        app_info.pEngineName = "No Engine";
        app_info.engineVersion = VK_MAKE_VERSION(1, 0, 0);
        app_info.apiVersion = VK_API_VERSION_1_0;

        const auto extensions = getRequiredExtensions();
        VkInstanceCreateInfo create_info{};
        create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        create_info.pApplicationInfo = &app_info;
        create_info.enabledExtensionCount = extensions.size();
        create_info.ppEnabledExtensionNames = extensions.data();
        if (validation_ == Validation::Enabled) {
            create_info.enabledLayerCount = static_cast<uint32_t>(validation_layers.size());
            create_info.ppEnabledLayerNames = validation_layers.data();
        } else {
            create_info.enabledLayerCount = 0;
        }
        if (vulkan_lib_->vkCreateInstance(&create_info, nullptr, &instance_) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create instance!");
        }
        vulkan_lib_.init(instance_);
    }

    void listExtensions() {
        uint32_t extension_count = 0;
        vulkan_lib_->vkEnumerateInstanceExtensionProperties(nullptr, &extension_count, nullptr);
        std::vector<VkExtensionProperties> extensions(extension_count);
        if (vulkan_lib_->vkEnumerateInstanceExtensionProperties(nullptr, &extension_count, extensions.data()) ==
            VK_SUCCESS) {
            std::cout << "Available extensions:\n";
            for (const auto& extension : extensions) {
                std::cout << '\t' << extension.extensionName << '\n';
            }
        }
    }

    void setupDebugMessenger() {
        if (validation_ != Validation::Enabled)
            return;

        VkDebugUtilsMessengerCreateInfoEXT create_info{};
        create_info.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
        create_info.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_FLAG_BITS_MAX_ENUM_EXT;
        create_info.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_FLAG_BITS_MAX_ENUM_EXT;
        create_info.pfnUserCallback = debugCallback;
        create_info.pUserData = this;  // to be able use reference to the app class in static function
        if (vulkan_lib_->vkCreateDebugUtilsMessengerEXT(instance_, &create_info, nullptr, &debug_messenger_) !=
            VK_SUCCESS) {
            throw std::runtime_error("Failed to set up debug messenger!");
        }
    }

    void createSurface() {
        if (SDL_Vulkan_CreateSurface(window_.get(), instance_, &surface_) != SDL_TRUE) {
            throw std::runtime_error("Failed to create window surface!");
        }
    }

    void pickPhysicalDevice() {
        uint32_t device_count = 0;
        vulkan_lib_->vkEnumeratePhysicalDevices(instance_, &device_count, nullptr);
        if (device_count == 0) {
            throw std::runtime_error("Failed to find GPUs with Vulkan support!");
        }
        std::vector<VkPhysicalDevice> devices(device_count);
        vulkan_lib_->vkEnumeratePhysicalDevices(instance_, &device_count, devices.data());

        for (const auto& device : devices) {
            if (isDeviceSuitable(device)) {
                physical_device_ = device;
                break;
            }
        }

        if (physical_device_ == VK_NULL_HANDLE) {
            throw std::runtime_error("failed to find a suitable GPU!");
        }
    }

    bool isDeviceSuitable(VkPhysicalDevice device) {
        VkPhysicalDeviceProperties device_properties;
        VkPhysicalDeviceFeatures device_features;
        vulkan_lib_->vkGetPhysicalDeviceProperties(device, &device_properties);
        vulkan_lib_->vkGetPhysicalDeviceFeatures(device, &device_features);

        const auto indices = findQueueFamilies(device);
        const auto extensions_supported = checkDeviceExtensionSupport(device);
        auto swap_chain_adequate = false;
        if (extensions_supported) {
            const auto swap_chain_support = querySwapChainSupport(device);
            swap_chain_adequate = !swap_chain_support.formats.empty() && !swap_chain_support.present_modes.empty();
        }

        return (device_properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU ||
                device_properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) &&
               device_features.geometryShader && indices.isComplete() && extensions_supported && swap_chain_adequate;
    }

    bool checkDeviceExtensionSupport(VkPhysicalDevice device) {
        uint32_t extension_count;
        vulkan_lib_->vkEnumerateDeviceExtensionProperties(device, nullptr, &extension_count, nullptr);

        std::vector<VkExtensionProperties> available_extensions(extension_count);
        vulkan_lib_->vkEnumerateDeviceExtensionProperties(device, nullptr, &extension_count,
                                                          available_extensions.data());

        std::set<std::string> required_extensions(device_extensions.begin(), device_extensions.end());

        for (const auto& extension : available_extensions) {
            required_extensions.erase(extension.extensionName);
        }

        return required_extensions.empty();
    }

    QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device) {
        QueueFamilyIndices indices;

        uint32_t queue_family_count = 0;
        vulkan_lib_->vkGetPhysicalDeviceQueueFamilyProperties(device, &queue_family_count, nullptr);

        std::vector<VkQueueFamilyProperties> queue_families(queue_family_count);
        vulkan_lib_->vkGetPhysicalDeviceQueueFamilyProperties(device, &queue_family_count, queue_families.data());

        for (uint32_t i = 0; (i < queue_families.size()) && !indices.isComplete(); i++) {
            if (queue_families[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
                indices.graphics_family = i;
            }
            VkBool32 present_support = VK_FALSE;
            vulkan_lib_->vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface_, &present_support);
            if (present_support) {
                indices.present_family = i;
            }
        }

        return indices;
    }

    SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device) {
        SwapChainSupportDetails details{};

        vulkan_lib_->vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface_, &details.capabilities);

        uint32_t format_count;
        vulkan_lib_->vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface_, &format_count, nullptr);
        if (format_count != 0) {
            details.formats.resize(format_count);
            vulkan_lib_->vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface_, &format_count, details.formats.data());
        }

        uint32_t present_mode_count;
        vulkan_lib_->vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface_, &present_mode_count, nullptr);
        if (present_mode_count != 0) {
            details.present_modes.resize(present_mode_count);
            vulkan_lib_->vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface_, &present_mode_count,
                                                                   details.present_modes.data());
        }
        return details;
    }

    VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& available_formats) {
        for (const auto& available_format : available_formats) {
            if (available_format.format == VK_FORMAT_B8G8R8A8_SRGB &&
                available_format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
                return available_format;
            }
        }
        return available_formats[0];
    }

    VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& available_present_modes) {
        for (const auto& available_present_mode : available_present_modes) {
            if (available_present_mode == VK_PRESENT_MODE_MAILBOX_KHR) {
                return available_present_mode;
            }
        }
        return VK_PRESENT_MODE_FIFO_KHR;
    }

    VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities) {
        if (capabilities.currentExtent.width != UINT32_MAX) {
            return capabilities.currentExtent;
        }

        int width = 0;
        int height = 0;
        SDL_Vulkan_GetDrawableSize(window_.get(), &width, &height);

        VkExtent2D actual_extent = {static_cast<uint32_t>(width), static_cast<uint32_t>(height)};

        actual_extent.width = std::max(capabilities.minImageExtent.width,
                                       std::min(capabilities.maxImageExtent.width, actual_extent.width));
        actual_extent.height = std::max(capabilities.minImageExtent.height,
                                        std::min(capabilities.maxImageExtent.height, actual_extent.height));

        return actual_extent;
    }

    void createLogicalDevice() {
        const float queue_priority = 1.0f;
        const auto indices = findQueueFamilies(physical_device_);

        std::vector<VkDeviceQueueCreateInfo> queue_create_infos;
        std::set<uint32_t> unique_queue_families = {indices.graphics_family.value(), indices.present_family.value()};
        for (const auto queue_family : unique_queue_families) {
            VkDeviceQueueCreateInfo queue_create_info{};
            queue_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            queue_create_info.queueFamilyIndex = queue_family;
            queue_create_info.queueCount = 1;
            queue_create_info.pQueuePriorities = &queue_priority;
            queue_create_infos.push_back(queue_create_info);
        }

        VkPhysicalDeviceFeatures device_features{};

        VkDeviceCreateInfo create_info{};
        create_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        create_info.pQueueCreateInfos = queue_create_infos.data();
        create_info.queueCreateInfoCount = static_cast<uint32_t>(queue_create_infos.size());
        create_info.pEnabledFeatures = &device_features;
        create_info.enabledExtensionCount = static_cast<uint32_t>(device_extensions.size());
        create_info.ppEnabledExtensionNames = device_extensions.data();
        if (validation_ == Validation::Enabled) {
            create_info.enabledLayerCount = static_cast<uint32_t>(validation_layers.size());
            create_info.ppEnabledLayerNames = validation_layers.data();
        } else {
            create_info.enabledLayerCount = 0;
        }
        if (vulkan_lib_->vkCreateDevice(physical_device_, &create_info, nullptr, &device_) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create logical device!");
        }

        vulkan_lib_->vkGetDeviceQueue(device_, indices.graphics_family.value(), 0, &graphics_queue_);
        vulkan_lib_->vkGetDeviceQueue(device_, indices.present_family.value(), 0, &present_queue_);
    }

    void createSwapChain() {
        const auto swap_chain_support = querySwapChainSupport(physical_device_);

        const auto surface_format = chooseSwapSurfaceFormat(swap_chain_support.formats);
        const auto present_mode = chooseSwapPresentMode(swap_chain_support.present_modes);
        const auto extent = chooseSwapExtent(swap_chain_support.capabilities);

        uint32_t image_count = swap_chain_support.capabilities.minImageCount + 1;
        if (swap_chain_support.capabilities.maxImageCount > 0 &&
            image_count > swap_chain_support.capabilities.maxImageCount) {
            image_count = swap_chain_support.capabilities.maxImageCount;
        }

        const auto indices = findQueueFamilies(physical_device_);
        uint32_t queue_family_indices[] = {indices.graphics_family.value(), indices.present_family.value()};

        VkSwapchainCreateInfoKHR create_info{};
        create_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        create_info.surface = surface_;
        create_info.minImageCount = image_count;
        create_info.imageFormat = surface_format.format;
        create_info.imageColorSpace = surface_format.colorSpace;
        create_info.imageExtent = extent;
        create_info.imageArrayLayers = 1;
        create_info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
        if (indices.graphics_family != indices.present_family) {
            create_info.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
            create_info.queueFamilyIndexCount = 2;
            create_info.pQueueFamilyIndices = queue_family_indices;
        } else {
            create_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
            create_info.queueFamilyIndexCount = 0;      // Optional
            create_info.pQueueFamilyIndices = nullptr;  // Optional
        }
        create_info.preTransform = swap_chain_support.capabilities.currentTransform;
        create_info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        create_info.presentMode = present_mode;
        create_info.clipped = VK_TRUE;
        create_info.oldSwapchain = VK_NULL_HANDLE;

        if (vulkan_lib_->vkCreateSwapchainKHR(device_, &create_info, nullptr, &swap_chain_) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create swap chain!");
        }

        vulkan_lib_->vkGetSwapchainImagesKHR(device_, swap_chain_, &image_count, nullptr);
        swap_chain_images_.resize(image_count);
        vulkan_lib_->vkGetSwapchainImagesKHR(device_, swap_chain_, &image_count, swap_chain_images_.data());

        swap_chain_image_format_ = surface_format.format;
        swap_chain_extent_ = extent;
    }

    void createImageViews() {
        swap_chain_image_views_.resize(swap_chain_images_.size());
        for (size_t i = 0; i < swap_chain_images_.size(); i++) {
            VkImageViewCreateInfo create_info{};
            create_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
            create_info.image = swap_chain_images_[i];
            create_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
            create_info.format = swap_chain_image_format_;
            create_info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
            create_info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
            create_info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
            create_info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
            create_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            create_info.subresourceRange.baseMipLevel = 0;
            create_info.subresourceRange.levelCount = 1;
            create_info.subresourceRange.baseArrayLayer = 0;
            create_info.subresourceRange.layerCount = 1;
            if (vulkan_lib_->vkCreateImageView(device_, &create_info, nullptr, &swap_chain_image_views_[i]) !=
                VK_SUCCESS) {
                throw std::runtime_error("Failed to create image views!");
            }
        }
    }

    void createRenderPass() {
        VkAttachmentDescription color_attachment{};
        color_attachment.format = swap_chain_image_format_;
        color_attachment.samples = VK_SAMPLE_COUNT_1_BIT;
        color_attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        color_attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        color_attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        color_attachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

        VkAttachmentReference color_attachment_ref{};
        color_attachment_ref.attachment = 0;
        color_attachment_ref.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        VkSubpassDescription subpass{};
        subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpass.colorAttachmentCount = 1;
        subpass.pColorAttachments = &color_attachment_ref;

        VkSubpassDependency dependency{};
        dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
        dependency.dstSubpass = 0;
        dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependency.srcAccessMask = 0;
        dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

        VkRenderPassCreateInfo render_pass_info{};
        render_pass_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        render_pass_info.attachmentCount = 1;
        render_pass_info.pAttachments = &color_attachment;
        render_pass_info.subpassCount = 1;
        render_pass_info.pSubpasses = &subpass;
        render_pass_info.dependencyCount = 1;
        render_pass_info.pDependencies = &dependency;

        if (vulkan_lib_->vkCreateRenderPass(device_, &render_pass_info, nullptr, &render_pass_) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create render pass!");
        }
    }

    VkShaderModule createShaderModule(const unsigned char* code, int size) {
        VkShaderModuleCreateInfo create_info{};
        create_info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        create_info.codeSize = size;
        create_info.pCode = reinterpret_cast<const uint32_t*>(code);

        VkShaderModule shader_module;
        if (vulkan_lib_->vkCreateShaderModule(device_, &create_info, nullptr, &shader_module) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create shader module!");
        }
        return shader_module;
    }

    void createGraphicsPipeline() {
        auto vert_shader_module = createShaderModule(shader_vert, shader_vert_length);
        auto frag_shader_module = createShaderModule(shader_frag, shader_frag_length);

        VkPipelineShaderStageCreateInfo vert_shader_stage_info{};
        vert_shader_stage_info.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        vert_shader_stage_info.stage = VK_SHADER_STAGE_VERTEX_BIT;
        vert_shader_stage_info.module = vert_shader_module;
        vert_shader_stage_info.pName = "main";

        VkPipelineShaderStageCreateInfo frag_shader_stage_info{};
        frag_shader_stage_info.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        frag_shader_stage_info.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
        frag_shader_stage_info.module = frag_shader_module;
        frag_shader_stage_info.pName = "main";

        VkPipelineShaderStageCreateInfo shaderStages[] = {vert_shader_stage_info, frag_shader_stage_info};

        VkPipelineVertexInputStateCreateInfo vertex_input_info{};
        vertex_input_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        vertex_input_info.vertexBindingDescriptionCount = 0;
        vertex_input_info.pVertexBindingDescriptions = nullptr;  // Optional
        vertex_input_info.vertexAttributeDescriptionCount = 0;
        vertex_input_info.pVertexAttributeDescriptions = nullptr;  // Optional

        VkPipelineInputAssemblyStateCreateInfo input_assembly{};
        input_assembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        input_assembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        input_assembly.primitiveRestartEnable = VK_FALSE;

        VkViewport viewport{};
        viewport.x = 0.0f;
        viewport.y = 0.0f;
        viewport.width = (float)swap_chain_extent_.width;
        viewport.height = (float)swap_chain_extent_.height;
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;

        VkRect2D scissor{};
        scissor.offset = {0, 0};
        scissor.extent = swap_chain_extent_;

        VkPipelineViewportStateCreateInfo viewport_state{};
        viewport_state.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        viewport_state.viewportCount = 1;
        viewport_state.pViewports = &viewport;
        viewport_state.scissorCount = 1;
        viewport_state.pScissors = &scissor;

        VkPipelineRasterizationStateCreateInfo rasterizer{};
        rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
        rasterizer.depthClampEnable = VK_FALSE;
        rasterizer.rasterizerDiscardEnable = VK_FALSE;
        rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
        rasterizer.lineWidth = 1.0f;
        rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
        rasterizer.frontFace = VK_FRONT_FACE_CLOCKWISE;
        rasterizer.depthBiasEnable = VK_FALSE;
        rasterizer.depthBiasConstantFactor = 0.0f;  // Optional
        rasterizer.depthBiasClamp = 0.0f;           // Optional
        rasterizer.depthBiasSlopeFactor = 0.0f;     // Optional

        VkPipelineMultisampleStateCreateInfo multisampling{};
        multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        multisampling.sampleShadingEnable = VK_FALSE;
        multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
        multisampling.minSampleShading = 1.0f;           // Optional
        multisampling.pSampleMask = nullptr;             // Optional
        multisampling.alphaToCoverageEnable = VK_FALSE;  // Optional
        multisampling.alphaToOneEnable = VK_FALSE;       // Optional

        VkPipelineColorBlendAttachmentState color_blend_attachment{};
        color_blend_attachment.colorWriteMask =
            VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
        color_blend_attachment.blendEnable = VK_FALSE;
        color_blend_attachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;   // Optional
        color_blend_attachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;  // Optional
        color_blend_attachment.colorBlendOp = VK_BLEND_OP_ADD;              // Optional
        color_blend_attachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;   // Optional
        color_blend_attachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;  // Optional
        color_blend_attachment.alphaBlendOp = VK_BLEND_OP_ADD;              // Optional

        VkPipelineColorBlendStateCreateInfo color_blending{};
        color_blending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        color_blending.logicOpEnable = VK_FALSE;
        color_blending.logicOp = VK_LOGIC_OP_COPY;  // Optional
        color_blending.attachmentCount = 1;
        color_blending.pAttachments = &color_blend_attachment;
        color_blending.blendConstants[0] = 0.0f;  // Optional
        color_blending.blendConstants[1] = 0.0f;  // Optional
        color_blending.blendConstants[2] = 0.0f;  // Optional
        color_blending.blendConstants[3] = 0.0f;  // Optional

        VkPipelineLayoutCreateInfo pipeline_layout_info{};
        pipeline_layout_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        pipeline_layout_info.setLayoutCount = 0;             // Optional
        pipeline_layout_info.pSetLayouts = nullptr;          // Optional
        pipeline_layout_info.pushConstantRangeCount = 0;     // Optional
        pipeline_layout_info.pPushConstantRanges = nullptr;  // Optional

        if (vulkan_lib_->vkCreatePipelineLayout(device_, &pipeline_layout_info, nullptr, &pipeline_layout_) !=
            VK_SUCCESS) {
            throw std::runtime_error("Failed to create pipeline layout!");
        }

        VkGraphicsPipelineCreateInfo pipelineInfo{};
        pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        pipelineInfo.stageCount = 2;
        pipelineInfo.pStages = shaderStages;
        pipelineInfo.pVertexInputState = &vertex_input_info;
        pipelineInfo.pInputAssemblyState = &input_assembly;
        pipelineInfo.pViewportState = &viewport_state;
        pipelineInfo.pRasterizationState = &rasterizer;
        pipelineInfo.pMultisampleState = &multisampling;
        pipelineInfo.pDepthStencilState = nullptr;  // Optional
        pipelineInfo.pColorBlendState = &color_blending;
        pipelineInfo.pDynamicState = nullptr;  // Optional
        pipelineInfo.layout = pipeline_layout_;
        pipelineInfo.renderPass = render_pass_;
        pipelineInfo.subpass = 0;
        pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;  // Optional
        pipelineInfo.basePipelineIndex = -1;               // Optional

        if (vulkan_lib_->vkCreateGraphicsPipelines(device_, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr,
                                                   &graphics_pipeline_) != VK_SUCCESS) {
            throw std::runtime_error("failed to create graphics pipeline!");
        }

        vulkan_lib_->vkDestroyShaderModule(device_, frag_shader_module, nullptr);
        vulkan_lib_->vkDestroyShaderModule(device_, vert_shader_module, nullptr);
    }

    void createFramebuffers() {
        swap_chain_framebuffers_.resize(swap_chain_image_views_.size());
        for (size_t i = 0; i < swap_chain_image_views_.size(); i++) {
            VkImageView attachments[] = {swap_chain_image_views_[i]};

            VkFramebufferCreateInfo framebuffer_info{};
            framebuffer_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
            framebuffer_info.renderPass = render_pass_;
            framebuffer_info.attachmentCount = 1;
            framebuffer_info.pAttachments = attachments;
            framebuffer_info.width = swap_chain_extent_.width;
            framebuffer_info.height = swap_chain_extent_.height;
            framebuffer_info.layers = 1;

            if (vulkan_lib_->vkCreateFramebuffer(device_, &framebuffer_info, nullptr, &swap_chain_framebuffers_[i]) !=
                VK_SUCCESS) {
                throw std::runtime_error("Failed to create framebuffer!");
            }
        }
    }

    void createCommandPool() {
        auto queue_family_indices = findQueueFamilies(physical_device_);

        VkCommandPoolCreateInfo pool_info{};
        pool_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        pool_info.queueFamilyIndex = queue_family_indices.graphics_family.value();
        pool_info.flags = 0;  // Optional

        if (vulkan_lib_->vkCreateCommandPool(device_, &pool_info, nullptr, &command_pool_) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create command pool!");
        }
    }

    void createCommandBuffers() {
        command_buffers_.resize(swap_chain_framebuffers_.size());

        VkCommandBufferAllocateInfo alloc_info{};
        alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        alloc_info.commandPool = command_pool_;
        alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        alloc_info.commandBufferCount = (uint32_t)command_buffers_.size();

        if (vulkan_lib_->vkAllocateCommandBuffers(device_, &alloc_info, command_buffers_.data()) != VK_SUCCESS) {
            throw std::runtime_error("failed to allocate command buffers!");
        }

        for (size_t i = 0; i < command_buffers_.size(); i++) {
            VkCommandBufferBeginInfo begin_info{};
            begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
            begin_info.flags = 0;                   // Optional
            begin_info.pInheritanceInfo = nullptr;  // Optional

            if (vulkan_lib_->vkBeginCommandBuffer(command_buffers_[i], &begin_info) != VK_SUCCESS) {
                throw std::runtime_error("failed to begin recording command buffer!");
            }

            VkRenderPassBeginInfo render_pass_info{};
            render_pass_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            render_pass_info.renderPass = render_pass_;
            render_pass_info.framebuffer = swap_chain_framebuffers_[i];
            render_pass_info.renderArea.offset = {0, 0};
            render_pass_info.renderArea.extent = swap_chain_extent_;
            VkClearValue clear_color = {0.0f, 0.0f, 0.0f, 1.0f};
            render_pass_info.clearValueCount = 1;
            render_pass_info.pClearValues = &clear_color;

            vulkan_lib_->vkCmdBeginRenderPass(command_buffers_[i], &render_pass_info, VK_SUBPASS_CONTENTS_INLINE);

            vulkan_lib_->vkCmdBindPipeline(command_buffers_[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphics_pipeline_);
            vulkan_lib_->vkCmdDraw(command_buffers_[i], 3, 1, 0, 0);

            vulkan_lib_->vkCmdEndRenderPass(command_buffers_[i]);

            if (vulkan_lib_->vkEndCommandBuffer(command_buffers_[i]) != VK_SUCCESS) {
                throw std::runtime_error("Failed to record command buffer!");
            }
        }
    }

    void createSyncObjects() {
        image_available_semaphores_.resize(MAX_FRAMES_IN_FLIGHT);
        render_finished_semaphores_.resize(MAX_FRAMES_IN_FLIGHT);
        in_flight_fences_.resize(MAX_FRAMES_IN_FLIGHT);
        images_in_flight_.resize(swap_chain_images_.size(), VK_NULL_HANDLE);

        VkSemaphoreCreateInfo semaphore_info{};
        semaphore_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

        VkFenceCreateInfo fence_info{};
        fence_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        fence_info.flags = VK_FENCE_CREATE_SIGNALED_BIT;

        for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
            if (vulkan_lib_->vkCreateSemaphore(device_, &semaphore_info, nullptr, &image_available_semaphores_[i]) !=
                    VK_SUCCESS ||
                vulkan_lib_->vkCreateSemaphore(device_, &semaphore_info, nullptr, &render_finished_semaphores_[i]) !=
                    VK_SUCCESS ||
                vulkan_lib_->vkCreateFence(device_, &fence_info, nullptr, &in_flight_fences_[i]) != VK_SUCCESS) {
                throw std::runtime_error("Failed to create synchronization objects for a frame!");
            }
        }
    }

    void drawFrame() {
        vulkan_lib_->vkWaitForFences(device_, 1, &in_flight_fences_[current_frame_], VK_TRUE, UINT64_MAX);

        uint32_t image_index{};
        const auto acquire_result = vulkan_lib_->vkAcquireNextImageKHR(device_, swap_chain_, UINT64_MAX,
                                                                       image_available_semaphores_[current_frame_],
                                                                       VK_NULL_HANDLE, &image_index);
        if (acquire_result == VK_ERROR_OUT_OF_DATE_KHR) {
            recreateSwapChain();
            return;
        } else if (acquire_result != VK_SUCCESS && acquire_result != VK_SUBOPTIMAL_KHR) {
            throw std::runtime_error("Failed to acquire swap chain image!");
        }

        // Check if a previous frame is using this image (i.e. there is its fence to wait on)
        if (images_in_flight_[image_index] != VK_NULL_HANDLE) {
            vulkan_lib_->vkWaitForFences(device_, 1, &images_in_flight_[image_index], VK_TRUE, UINT64_MAX);
        }
        // Mark the image as now being in use by this frame
        images_in_flight_[image_index] = in_flight_fences_[current_frame_];

        VkSubmitInfo submit_info{};
        submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        VkSemaphore wait_semaphores[] = {image_available_semaphores_[current_frame_]};
        VkPipelineStageFlags wait_stages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
        submit_info.waitSemaphoreCount = 1;
        submit_info.pWaitSemaphores = wait_semaphores;
        submit_info.pWaitDstStageMask = wait_stages;
        submit_info.commandBufferCount = 1;
        submit_info.pCommandBuffers = &command_buffers_[image_index];
        VkSemaphore signal_semaphores[] = {render_finished_semaphores_[current_frame_]};
        submit_info.signalSemaphoreCount = 1;
        submit_info.pSignalSemaphores = signal_semaphores;

        vulkan_lib_->vkResetFences(device_, 1, &in_flight_fences_[current_frame_]);
        if (vulkan_lib_->vkQueueSubmit(graphics_queue_, 1, &submit_info, in_flight_fences_[current_frame_]) !=
            VK_SUCCESS) {
            throw std::runtime_error("Failed to submit draw command buffer!");
        }

        VkPresentInfoKHR present_info{};
        present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        present_info.waitSemaphoreCount = 1;
        present_info.pWaitSemaphores = signal_semaphores;
        VkSwapchainKHR swap_chains[] = {swap_chain_};
        present_info.swapchainCount = 1;
        present_info.pSwapchains = swap_chains;
        present_info.pImageIndices = &image_index;
        present_info.pResults = nullptr;  // Optional

        const auto present_result = vulkan_lib_->vkQueuePresentKHR(present_queue_, &present_info);
        if (present_result == VK_ERROR_OUT_OF_DATE_KHR || present_result == VK_SUBOPTIMAL_KHR || window_resized_) {
            window_resized_ = false;
            recreateSwapChain();
        } else if (present_result != VK_SUCCESS) {
            throw std::runtime_error("Failed to present swap chain image!");
        }

        // vulkan_lib_->vkQueueWaitIdle(present_queue_);
        current_frame_ = (current_frame_ + 1) % MAX_FRAMES_IN_FLIGHT;
    }

    void mainLoop() {
        bool running = true;
        SDL_Event event{};
        while (running) {
            while (SDL_PollEvent(&event)) {
                if (event.type == SDL_QUIT) {
                    running = false;
                } else if (event.type == SDL_WINDOWEVENT) {
                    windowEventHandle(event.window);
                }
            }
            drawFrame();
        }

        vulkan_lib_->vkDeviceWaitIdle(device_);
    }

    void cleanup() {
        cleanupSwapChain();

        for (auto semaphore : render_finished_semaphores_) {
            vulkan_lib_->vkDestroySemaphore(device_, semaphore, nullptr);
        }
        render_finished_semaphores_.clear();

        for (auto semaphore : image_available_semaphores_) {
            vulkan_lib_->vkDestroySemaphore(device_, semaphore, nullptr);
        }
        image_available_semaphores_.clear();

        for (auto fence : in_flight_fences_) {
            vulkan_lib_->vkDestroyFence(device_, fence, nullptr);
        }
        in_flight_fences_.clear();
        images_in_flight_.clear();
        current_frame_ = 0;

        if (command_pool_) {
            vulkan_lib_->vkDestroyCommandPool(device_, command_pool_, nullptr);
            command_pool_ = nullptr;
        }

        if (device_ != VK_NULL_HANDLE) {
            vulkan_lib_->vkDestroyDevice(device_, nullptr);
            device_ = VK_NULL_HANDLE;
        }

        if (validation_ == Validation::Enabled && debug_messenger_ != VK_NULL_HANDLE) {
            vulkan_lib_->vkDestroyDebugUtilsMessengerEXT(instance_, debug_messenger_, nullptr);
            debug_messenger_ = VK_NULL_HANDLE;
        }

        if (surface_ != VK_NULL_HANDLE) {
            vulkan_lib_->vkDestroySurfaceKHR(instance_, surface_, nullptr);
            surface_ = VK_NULL_HANDLE;
        }

        if (instance_ != VK_NULL_HANDLE) {
            vulkan_lib_->vkDestroyInstance(instance_, nullptr);
            instance_ = VK_NULL_HANDLE;
        }
    }

 private:
    const Validation validation_;
    SdlInit sdl_init_;
    VulkanLibrary vulkan_lib_;
    std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)> window_;

    VkInstance instance_ = VK_NULL_HANDLE;
    VkDebugUtilsMessengerEXT debug_messenger_ = VK_NULL_HANDLE;
    VkSurfaceKHR surface_ = VK_NULL_HANDLE;
    VkPhysicalDevice physical_device_ = VK_NULL_HANDLE;
    VkDevice device_ = VK_NULL_HANDLE;
    VkQueue graphics_queue_ = VK_NULL_HANDLE;
    VkQueue present_queue_ = VK_NULL_HANDLE;
    VkSwapchainKHR swap_chain_ = nullptr;
    std::vector<VkImage> swap_chain_images_;
    VkFormat swap_chain_image_format_ = VK_FORMAT_UNDEFINED;
    VkExtent2D swap_chain_extent_{};
    std::vector<VkImageView> swap_chain_image_views_;
    VkRenderPass render_pass_ = nullptr;
    VkPipelineLayout pipeline_layout_ = nullptr;
    VkPipeline graphics_pipeline_ = nullptr;
    std::vector<VkFramebuffer> swap_chain_framebuffers_;
    VkCommandPool command_pool_ = nullptr;
    std::vector<VkCommandBuffer> command_buffers_;
    std::vector<VkSemaphore> image_available_semaphores_;
    std::vector<VkSemaphore> render_finished_semaphores_;
    std::vector<VkFence> in_flight_fences_;
    std::vector<VkFence> images_in_flight_;
    size_t current_frame_ = 0;
    bool window_resized_ = false;
    bool window_minimized_ = true;
};

const std::vector<const char*> HelloTriangleApplication::validation_layers = {
    "VK_LAYER_KHRONOS_validation",
    //"VK_LAYER_LUNARG_standard_validation",
};

const std::vector<const char*> HelloTriangleApplication::device_extensions = {
    VK_KHR_SWAPCHAIN_EXTENSION_NAME,
};

int main(int argc, char* argv[]) {
    try {
        HelloTriangleApplication app(Validation::Disabled);
        app.run();
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return FAILURE;
    }
    return SUCCESS;
}